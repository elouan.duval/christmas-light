//#region Start and stop lightning
function start()
{
    var start = document.getElementById('start');
    var stop = document.getElementById('stop');
    startLight();
    start.disabled = true;
    stop.disabled = false;
}

function stop()
{
    var stop = document.getElementById('stop');
    var start = document.getElementById('start');
    stop.disabled = true;
    start.disabled = false;
}
//#endregion
//#region Show between 1 and 7 lines
function showRow()
{
    numberInput = document.getElementById('number');
    number = numberInput.value;
    if(number == 1){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'hidden';
        document.getElementById("l3").style.visibility= 'hidden';
        document.getElementById("l4").style.visibility= 'hidden';
        document.getElementById("l5").style.visibility= 'hidden';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 2){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'hidden';
        document.getElementById("l4").style.visibility= 'hidden';
        document.getElementById("l5").style.visibility= 'hidden';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 3){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'visible';
        document.getElementById("l4").style.visibility= 'hidden';
        document.getElementById("l5").style.visibility= 'hidden';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 4){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'visible';
        document.getElementById("l4").style.visibility= 'visible';
        document.getElementById("l5").style.visibility= 'hidden';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 5){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'visible';
        document.getElementById("l4").style.visibility= 'visible';
        document.getElementById("l5").style.visibility= 'visible';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 6){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'visible';
        document.getElementById("l4").style.visibility= 'visible';
        document.getElementById("l5").style.visibility= 'visible';
        document.getElementById("l6").style.visibility= 'visible';
        document.getElementById("l7").style.visibility= 'hidden';
    }
    else if(number == 7){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'visible';
        document.getElementById("l3").style.visibility= 'visible';
        document.getElementById("l4").style.visibility= 'visible';
        document.getElementById("l5").style.visibility= 'visible';
        document.getElementById("l6").style.visibility= 'visible';
        document.getElementById("l7").style.visibility= 'visible';
    }
    else if(number != 1 && number != 2 && number != 3 && number != 4 && number != 5 && number != 6 && number != 7){
        document.getElementById("l1").style.visibility= 'visible';
        document.getElementById("l2").style.visibility= 'hidden';
        document.getElementById("l3").style.visibility= 'hidden';
        document.getElementById("l4").style.visibility= 'hidden';
        document.getElementById("l5").style.visibility= 'hidden';
        document.getElementById("l6").style.visibility= 'hidden';
        document.getElementById("l7").style.visibility= 'hidden';
    }
}
//#endregion
//#region Bright of lights

var circle_lighting_time = 1000; // La durée d'eclairage de chaque cercle
var brightness = 100;

function startLight(){
    const ci1 = document.getElementById("l1").getElementsByClassName("circle"); // HTMLCollection of all classes of cercle
    const ci2 = document.getElementById("l2").getElementsByClassName("circle");
    const ci3 = document.getElementById("l3").getElementsByClassName("circle");
    const ci4 = document.getElementById("l4").getElementsByClassName("circle");
    const ci5 = document.getElementById("l5").getElementsByClassName("circle");
    const ci6 = document.getElementById("l6").getElementsByClassName("circle");
    const ci7 = document.getElementById("l7").getElementsByClassName("circle");
    const circles1 = Array.prototype.slice.call(ci1); // transformation de HTMLCollection en Array
    const circles2 = Array.prototype.slice.call(ci2);
    const circles3 = Array.prototype.slice.call(ci3);
    const circles4 = Array.prototype.slice.call(ci4);
    const circles5 = Array.prototype.slice.call(ci5);
    const circles6 = Array.prototype.slice.call(ci6);
    const circles7 = Array.prototype.slice.call(ci7);
    const DISPLAY_NUMBER = 10; // Combien de fois l'expo est repeté
    const CIRCLE_LIGHTING_TIME = circle_lighting_time; // La durée d'eclairage de chaque cercle
    var j = 0;
    
    function theDisplay(){
        circles1.map((circle,i) => {  // iteration pour chaque circle dans circles
            setTimeout(function() {
                i>0?circles1[i-1].style.filter = "brightness(30%)":null; // Aprés la deuxieme cercle on eteint le predecesseur de cette cercle
                i>0?circles1[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";// allume la cercle i-éme
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";

            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles2.map((circle,i) => {
            setTimeout(function() {
                i>0?circles2[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles2[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles3.map((circle,i) => {
            setTimeout(function() {
                i>0?circles3[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles3[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles4.map((circle,i) => {
            setTimeout(function() {
                i>0?circles4[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles4[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles5.map((circle,i) => {
            setTimeout(function() {
                i>0?circles5[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles5[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles6.map((circle,i) => {
            setTimeout(function() {
                i>0?circles6[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles6[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
        circles7.map((circle,i) => {
            setTimeout(function() {
                i>0?circles7[i-1].style.filter = "brightness(30%)":null; 
                i>0?circles7[i-1].style.animation="none":null; 
                circle.style.filter = "brightness("+brightness+"%)";
                circle.style.animation="bulb-pulse-1 0.8s linear 0s infinite alternate";
            }, CIRCLE_LIGHTING_TIME * i);
        }
        );
    }
    
    while(j<DISPLAY_NUMBER){ //repéte le display "DISPLAY_NUMBER" fois
        setTimeout(function() {
            theDisplay();
            circles1[circles1.length-1].style.filter = "brightness(30%)"; // eteint la derniere circle pour passer a le display suivant
            circles1[circles1.length-1].style.animation="none";
            circles2[circles2.length-1].style.filter = "brightness(30%)";
            circles2[circles2.length-1].style.animation="none";
            circles3[circles3.length-1].style.filter = "brightness(30%)";
            circles3[circles3.length-1].style.animation="none";
            circles4[circles4.length-1].style.filter = "brightness(30%)";
            circles4[circles4.length-1].style.animation="none";
            circles5[circles5.length-1].style.filter = "brightness(30%)";
            circles5[circles5.length-1].style.animation="none";
            circles6[circles6.length-1].style.filter = "brightness(30%)";
            circles6[circles6.length-1].style.animation="none";
            circles7[circles7.length-1].style.filter = "brightness(30%)";
            circles7[circles7.length-1].style.animation="none";
        }, 
        circles1.length * 1000 * j,
        circles2.length * 1000 * j,
        circles3.length * 1000 * j,
        circles4.length * 1000 * j,
        circles5.length * 1000 * j,
        circles6.length * 1000 * j,
        circles7.length * 1000 * j,);
        j++;
    }
}
function stopLight(){}//to do

function setBrightness(intensity){
	brightness=intensity.value;
}

function setCircleLightingTime(time){
	circle_lighting_time=time.value;
}
//#endregion
//#region Size
var slider = document.getElementById("size");
const c1 = document.getElementById("l1");
const c2 = document.getElementById("l2");
const c3 = document.getElementById("l3");
const c4 = document.getElementById("l4");
const c5 = document.getElementById("l5");
const c6 = document.getElementById("l6");
const c7 = document.getElementById("l7");
const acircles1 = Array.prototype.slice.call( c1.getElementsByClassName("circle") );
const acircles2 = Array.prototype.slice.call( c2.getElementsByClassName("circle") );
const acircles3 = Array.prototype.slice.call( c3.getElementsByClassName("circle") );
const acircles4 = Array.prototype.slice.call( c4.getElementsByClassName("circle") );
const acircles5 = Array.prototype.slice.call( c5.getElementsByClassName("circle") );
const acircles6 = Array.prototype.slice.call( c6.getElementsByClassName("circle") );
const acircles7 = Array.prototype.slice.call( c7.getElementsByClassName("circle") );

slider.value=50;

slider.oninput = function() {
    acircles1.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles2.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles3.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles4.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles5.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles6.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
    acircles7.map((circle,i)=>{
        console.log(circle);
        circle.style.width = slider.value+"px";
        circle.style.height = slider.value+"px";
    });
}
//#endregions
