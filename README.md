# Christmas Light
**Mardi 19/10 :**
- Basé sur ce site : https://www.freecodecamp.org/news/here-are-some-app-ideas-you-can-build-to-level-up-your-coding-skills-39618291f672/
- Modéliser l'application
- Définir toutes les tâches à faire
- Création du projet sur gitlab
- Création d'une Milestone et des Issues
- Répartition des tâches
**Mercredi 20/10 :**
- Développement des différentes tâches
- Problème d'intégration du CI -> Mauvais token utilisé
- Commit et merge des différentes branches 
**Jeudi 21/10 :**
- Finalisation du developpement
- Réalisation des tâches de tests
- Pousser les dernières branches sur la principale
- Vérifier que tout fonctionne bien
# The Application
- Cette application permet d'afficher des guirlandes de lumière qui sont paramétrables
- Un bouton "Start" qui permet d'allumer l'éclairage de la guirlande
- Un bouton "stop" censé arrêter l'éclairage(pas encore en place)
- Un textbox pour que l'utilisateur choisissent le nombre de lignes de guirlandes à affichée, entre 1 et 7
- Une barre qui permet de définir la taille des lumières des guirlandes
- Une barre qui permet de choisir l'intensité de la lumière
- Une barre qui permet de choisir la durée de chaque lumière sur la guirlande
